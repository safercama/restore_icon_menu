{
    'name': 'Restore Icon Menu',
    'version': '12.0.1',
    'category': 'Tools',
    'author': 'Saulo Castillo',
    'summary': 'Restore icon menu when a database was restored',
    'website': 'safecastillo@gmail.com',
    'description': """
        - Clicked button 'restore icon menu' in settings/general options
     """,
    'depends': [],
    'data': [
        'views/res_config_settings_views.xml',        
    ],   
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'AGPL-3',
}
