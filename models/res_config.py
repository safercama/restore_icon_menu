# coding: utf-8
from odoo import api, fields, models, _

class ResConfigSettings(models.TransientModel):
	_inherit = 'res.config.settings'

	@api.multi
	def action_restore_icon_menu(self):
		menu_parents = self.env['ir.ui.menu'].search([('parent_id','=',False),('web_icon','!=',False)])

		for menu in menu_parents:
			if menu.web_icon:
				menu.write({'web_icon_data' : menu._compute_web_icon_data(menu.web_icon)})

		
	
   
   